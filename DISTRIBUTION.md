# Distribution
The distribution is handled by Gitlab CI. For manual distribution please follow the following steps.

Make sure you have the latest version of `setuptools` and `wheels` installed:

    python3 -m pip install --user --upgrade setuptools wheel

Now run this command from the same directory as where `setup.py` is located:

    rm -Rf dist/ && python3 setup.py sdist bdist_wheel

To test the package locally, use:

    python3 -m pip install -e .

Now make sure you have the latest version of Twine installed:

    python3 -m pip install --user --upgrade twine

Use the following command to upload the package to `test.pypi.org`:

    python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

Use this command to upload the package to `pypi.org`:

    python3 -m twine upload dist/*

